import { fetchEventSource } from 'https://cdn.jsdelivr.net/npm/@microsoft/fetch-event-source@2.0.1/+esm';

const uploadProgress = document.getElementById('uploadProgress')
const outputBox = document.getElementById('outputBox')

class EncodeFailed extends Error {}
class EncodeCompleted {}

document.getElementById('inputForm').onsubmit = async function handleFile(evt) {
	evt.preventDefault();
	const fileInput = document.getElementById('fileSelect');
	if (fileInput.files.length < 1) {
		console.log('No files selected; doing nothing');
	}
	const file = fileInput.files[0];

	uploadProgress.value = 0;
	outputBox.innerHTML = '';

	let resultData = null;
	let resultDataOffset = null;

	try {
		// fetchEventSource() resolves on stream completion
		await fetchEventSource('squish', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/octet-stream',
			},
			body: file,
			onmessage(ev) {
				switch (ev.event) {
					case 'uploadprogress':
						uploadProgress.value = ev.data;
						break;
					case 'stdout':
						outputBox.append(ev.data, '\n')
						outputBox.scrollTo(0, outputBox.scrollHeight)
						break;
					case 'returncode':
						uploadProgress.value = 0;
						const code = Number(ev.data);
						if (code !== 0) {
							throw new EncodeFailed()
						}
						break;
					case 'resultsize':
						resultData = new Uint8Array(Number(ev.data));
						resultDataOffset = 0;
						break;
					case 'result':
						const chunk = atob(ev.data);
						for (let i = 0; i < chunk.length; i++) {
							resultData[resultDataOffset++] = chunk[i].codePointAt(0);
						}
						uploadProgress.value = resultDataOffset / resultData.length;
						console.log('Now have %d bytes of data (expecting %d)', resultDataOffset, resultData.length);

						if (resultDataOffset === resultData.length) {
							const f = new File([resultData], file.name + '.mp4', {type: 'video/mp4'});
							const link = document.createElement('a')
							link.download = f.name;
							link.href = URL.createObjectURL(f);
							resultData = null;
							resultDataOffset = null;
							const clickHandler = () => {
								setTimeout(() => {
									URL.revokeObjectURL(link.href);
									link.removeEventListener('click', clickHandler);
								}, 150);
							};
							link.addEventListener('click', clickHandler);
							link.click();
							throw new EncodeCompleted();
						}
						break;
					default:
						console.log('Unhandled event type: %o', ev.event);
						break;
				}
			},
			onerror(err) {
				throw err;
			},
			openWhenHidden: true,
		});
	} catch (e) {
		if (e instanceof EncodeCompleted) {
			outputBox.append('All done!')
			fileInput.value = null;
		} else if (e instanceof EncodeFailed) {
			console.log('exploded');
		}
	}
};
