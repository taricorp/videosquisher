#!/usr/bin/env python
#
# SPDX-License-Identifier: BSD-2-Clause

import base64
import http.server
import os
import selectors
import subprocess
import tempfile

BUF_COPY_SZ = 1 << 20


class SquishHandler(http.server.SimpleHTTPRequestHandler):
    def send_event(self, data: str = None, ty=None):
        if data is not None:
            if ty is not None:
                self.wfile.write(f"event: {ty}\n".encode("utf-8"))
            for line in data.splitlines():
                self.wfile.write(f"data: {line}\n".encode("utf-8"))
            self.wfile.write(b"\n")
        else:
            self.wfile.write(b":\n")
        self.wfile.flush()

    def handle_subprocess(self, proc: subprocess.Popen):
        with selectors.DefaultSelector() as sel:
            os.set_blocking(proc.stdout.fileno(), False)
            sel.register(proc.stdout, selectors.EVENT_READ)
            while True:
                ready = sel.select(timeout=1)
                if ready:
                    for key, events in ready:
                        # If there's data available, read it; we know
                        # there's data ready so this shouldn't block as
                        # long as we specify a size.
                        if key.fileobj == proc.stdout:
                            data = proc.stdout.read(16384)
                            if not data:
                                return
                            self.send_event(data, ty="stdout")
                else:
                    self.send_event()

    def do_POST(self):
        if self.headers.get("Content-Type") != "application/octet-stream":
            self.send_error(
                400, "Wrong type", "POSTs must have type application/octet-stream"
            )
            return

        upload_size = self.headers.get("Content-Length")
        if upload_size is None:
            self.send_error(
                400, "Missing length", "Content-Length must be set for POSTs"
            )
            return
        upload_size = int(upload_size)

        self.send_response(200)
        self.send_header("Content-Type", "text/event-stream")
        self.send_header("Cache-Control", "no-store")
        self.end_headers()

        with tempfile.NamedTemporaryFile(mode="wb", dir="/var/tmp") as infile:
            for o in range(0, upload_size, BUF_COPY_SZ):
                buf = self.rfile.read(min(BUF_COPY_SZ, upload_size - o))
                infile.write(buf)
                self.send_event(f"{o / upload_size:.02}", "uploadprogress")
            self.send_event("1", "uploadprogress")

            with tempfile.NamedTemporaryFile(mode="rb", dir="/var/tmp") as outfile:
                args = [
                    # Output mp4 with leading MOOV
                    "--format",
                    "av_mp4",
                    "--optimize",
                    # 5 megabits per second video, two-pass encoding
                    "--multi-pass",
                    "--vb",
                    "5000",
                    "--crop-mode",
                    "none",
                    # QSV (Intel hardware-accelerated) encoder
                    "--encoder",
                    "qsv_h264",
                    "--encoder-preset",
                    "quality",
                    # or x264
                    #'--encoder', 'x264', '--encoder-preset', 'slow', '--turbo',
                    # Don't process audio, just copy
                    "--aencoder",
                    "copy",
                    "--input",
                    infile.name,
                    "--output",
                    outfile.name,
                ]
                with subprocess.Popen(
                    ["HandBrakeCLI"] + args,
                    stdin=subprocess.DEVNULL,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    text=True,
                ) as proc:
                    self.handle_subprocess(proc)
                    assert proc.poll() is not None, "Subprocess should have exited"
                    self.send_event(str(proc.returncode), "returncode")

                outfile.seek(0, os.SEEK_END)
                output_len = outfile.tell()
                outfile.seek(0, os.SEEK_SET)
                self.send_event(f"{output_len}", "resultsize")

                # Send the output file back to the client
                while chunk := outfile.read(BUF_COPY_SZ):
                    self.send_event(base64.b64encode(chunk).decode("ascii"), "result")


# curl -X POST --data-binary @file --header "Content-Type: application/octet-stream" http://localhost:9429/squish
if __name__ == "__main__":
    PORT = 9428

    httpd = http.server.HTTPServer(("", PORT), SquishHandler)
    print("serving on port", PORT)
    httpd.serve_forever()
