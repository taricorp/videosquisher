# Video Squisher

This is a small web application for transcoding videos, built on the web
server-sent events API.

Run the server as `python server.py` and load the reported page in a
web browser. Regular `GET` requests will return the contents of files in
the working directory, so generally you want the working directory of
the server to be the one that contains `index.html` and `client.mjs`.

A longer-form description of how the application was designed and interesting
aspects of the implementation is at https://www.taricorp.net/2023/video-squisher/

## Security

This program should not be used for public applications without careful
consideration of its potential security implications. `http.server`'s
request handlers implement only rudimentary security checks and accepting
unverified video input from users could easily exhaust a server's available
storage or take advantage of bugs in the encoder to execute malicious code,
among other possible issues.

## License

Copyright (c) 2023 Peter Marheine <peter@taricorp.net>

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

